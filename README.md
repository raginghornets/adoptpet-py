# adoptapet-py

## Description
This program was originally made to teach my younger sister about programming in Python.

## How to use this program on your own computer
1. [Download Git](https://git-scm.com/downloads).
2. [Download Python 3](https://www.python.org/downloads/) (Make sure you check the "Add to Python PATH" checkbox).
3. [Open terminal or command prompt](https://askubuntu.com/questions/38162/what-is-a-terminal-and-how-do-i-open-and-use-it#38220) on your computer.
4. Type `git clone https://github.com/airicbear/adoptpet-py.git` in terminal to clone the repository onto your computer.
5. Type `cd adoptpet-py` to change your terminal's current location to the clone's location.
6. Type `python app.py` in terminal.
7. Follow the instructions given by the app.
8. (optional) To delete the cloned repository, type `cd ..` and then `rm -rf adoptpet-py`.
9. Congratulations! You have successfully used a Python program from GitHub. Feel free to change the code on your own to make it do different things! :)