#!/usr/bin/env python3

# Keep asking for an integer until the input is an integer
def integer(prompt="integer: "):
  userInput = input(prompt)
  try:
    return int(userInput)
  except:
    print("Please enter a number.")
    return integer(prompt)

# This is what happens when you run "python get.py"
if __name__ == "__main__":
  test = [integer()]
  print("\n".join([item for item in test]))