#!/usr/bin/env python3

def main():
  import petSmart
  petSmart.adoptPet()

def quit():
  try:
    import sys
    sys.exit(0)
  except SystemExit:
    import os
    os._exit(0)

# This is what happens when you run "python app.py"
if __name__ == "__main__":
  try:
    main()
  except:
    quit()